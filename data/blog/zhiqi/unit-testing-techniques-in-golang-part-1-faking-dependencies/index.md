---
title: Unit Testing Techniques in Golang Part 1 – Faking Dependencies
createdDate: '2015-10-26'
updatedDate: '2017-10-10'
author: zhiqi
tags:
  - fake
  - mock
  - golang
  - unit test
image: AAEAAQAAAAAAAAcqAAAAJGQ0MTBkZmQwLWM1NTktNGQxMi1iNmNjLWJhZmFiZDk5Njc3MQ.png
draft: false
canonical: null
excerpt: >-
  Golang has consicse official documents and powerful testing tools. However,
  faking/mocking dependencies is not easy and/or straight forward.
---

Golang has very concise yet self-contained official documents, tutorials and blog posts for new comers. For testing, they have good materials, for instance <https://www.golang-book.com/books/intro/12> and <https://golang.org/pkg/testing/>, showing how to use the built-in testing framework. However when comes to the question of how to write testable codes in production code and how to swap dependencies out in testing code in Golang’s style, new comers have to look for other resources. Till now, there seems to be still missing a comprehensive enough material for new comers. That’s why this post is created.

Testable production code means the callees(function/method) of the function/method under testing can be easily faked or so called swapped out. The basic idea behind this is, in test code, we want to replace the production implementation of the callee with test implementation that designed to assert the input arguments(mocking), to report how many times the callees are actually invoked(spying), or to return some fixed values(stubbing).

### Fake functions

As Golang’s function is first class function, it is quite easy to be faked as long as it is **invoked on variable instead of name**.  
Consider the following example of non-testable code:  

The logic of this piece of code is that function `ModifyUserName` should invoke function `UpdateName` if the `newName` paramenter is not empty. However we can not test this logic because we can not fake function `UpdateName`. To solve it, we only need to change the way how it is declared:  

After this change, the faking and the test become possible:  

Executable test here: <https://play.golang.org/p/P6Fv1KcBBS>  
One might ask what if the callee function is from a third-party package and not declared on variable? Yes, it is completely possible. As what we mentioned before, invoke it on variable rather than name. Just declare a variable storing the value of the callee function and then do the invocation on the variable.

### Fake methods

For callee which is a method, the approach is **using interface rather than concret type**. Consider the same example as before but modified to use method :  

The logic of this piece of code is that function `ModifyUserName` should invoke method `UpdateName` of structure type `MySession` if the `name` parameter is not empty. With current implementation, we can not replace `UpdateName` with another implementation. The solution is, when defining the `ModifyUserName`, we specify the input parameter as an interface instead of the concret structure type. So the above code will be changed into this(implementation of type MySession is not changed):  

After this change, the faking and the test become possible:  

Executable test here: <https://play.golang.org/p/HMWcnmQe2J>  
With the above example, we know that we should create an interface for the invoked methods if there is no interface defined for them. What if there’s already an interface defined for them? The answer depends on how big this interface is and how many methods are the dependencies of the testing target. If the interface is quite small or most of the methods are the dependencies, then it is OK to reuse the interface. If the interface has like 50 methods but only 2 of them are the dependencies, you’d better define an interface for that 2 methods, otherwise, you have to implement all of the 50 methods which is not a happy job.  
There is another approach we also see in practice, which is wrapping a method in a function variable, and then invoke on the function variable instead of the method:  

Executable test here: <https://play.golang.org/p/YjaNG2GjFj>

### Summary

In this post, we answered a couple of questions of dependency faking techniques in Golang. You may go through these questions again to see if you’ve internalized the ideas yet.

-   how to declare a function that is easy to be faked?
-   what to do if a third-party function is not declared on variable?
-   how to fake methods of a non-interface type?
-   how to fake methods of an interface?
