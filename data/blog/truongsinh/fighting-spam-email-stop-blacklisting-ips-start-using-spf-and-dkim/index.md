---
title: 'Fighting spam email: stop blacklisting IPs, start using SPF and DKIM'
createdDate: '2015-10-25'
updatedDate: '2017-10-10'
author: truongsinh
tags:
  - email
  - dkim
  - spf
  - spam
  - pki
image: william-iven-8515.jpg
draft: false
canonical: >-
  https://www.linkedin.com/pulse/fighting-spam-email-stop-blacklisting-ips-start-using-tran-nguyen
excerpt: >-
  in contrast to IP addresses for whitelist/blacklist, SPF and DKIM is modern
  way to fight spam
---

_[Also published on LinkedIn](https://www.linkedin.com/pulse/fighting-spam-email-stop-blacklisting-ips-start-using-tran-nguyen)_

Let’s face it, using <abbr title="Internet Protocol">IP</abbr> address as a method of identification/authentication (and thus fighting spam/fraud/scam) is old-fashioned, so old-fashioned! It dates back to the 90s, when any tech-startup must have at least a hardware guy, who spends his whole time shopping gears, setting them up, contacting <abbr title="internet service provider">ISP</abbr> to open port 25 for <abbr title="Simple Mail Transfer Protocol">SMTP</abbr>, watching out for overheat servers that can grill up a whole building, etc. Even during the the 2000s, some people were still [laughing at cloud computing](https://www.youtube.com/watch?v=0FacYAI6DY0) ([for which they had to pay](http://fortune.com/2015/09/16/oracle-sees-big-cloud-growth/)).

Hey grandpa, welcome to the 2010s, in which everybody (except giants who have their own heaven) is using cloud now, be it AWS, Google Cloud, Microsoft Azure, Digital Ocean, or thousands of other providers. Of course with cloud computing, we can purchase anything, including IPs, but in case you are living on credit card debt, [IP (v4) addresses are exhausting](https://en.wikipedia.org/wiki/IPv4_address_exhaustion) (and don’t you dare tell me about IPv6). Stop wasting internet resources and start making sense, will ya?

So, we’re using cloud, we don’t want to waste IP addresses, but we want to fight email spams, is it even possible? C’mon, it has been possible ever since the mid-2000s, just use [SPF](https://en.wikipedia.org/wiki/Sender_Policy_Framework). In a nutshell, instead of having black-/white-lists of IPs from which emails are sent, [email agents](https://en.wikipedia.org/wiki/Email_agent_(infrastructure)) shall have black-/white-lists of domains. Periodically and/or upon receiving an email, email agents shall evaluate its domain’s SPF value to determine whether it was sent from (one of) legitimate servers. Yay, no hard-code IPs list needed!

Beware, SPF has [its own caveats](https://en.wikipedia.org/wiki/Sender_Policy_Framework#Caveats), one of which is important if you are using shared email sending services. Imagine that you are using service provided by EmailGatling, and configure your `mydomain.example` SPF to `include: _spf.emailgatling.example`, which resolves as `193.168.1.1`. All emails will be sent (“genuine@mydomain.example”, by you, via EmailGatling) from server with IP address `193.168.1.1`, and thus look legitimate. Out of nowhere, Mr. YourNightmare also starts using EmailGatling, and discovers that he can easily use EmailGatling to imitate your service. All emails will be sent (“your_nightmare@mydomain.example”, by Mr. YourNightmare, via EmailGatling) from server with IP address `193.168.1.1`, and thus also look legitimate (remember, looks can be deceiving).

To fight scam/fraud in the above scenario, we can use [DKIM](https://en.wikipedia.org/wiki/DomainKeys_Identified_Mail). It’s simply utilizing [public key infrastructure](https://en.wikipedia.org/wiki/Public_key_infrastructure). Yeah, grandpas know PKI, but in case you get lost into this post and still read this sentence, I have a simple analogous example for you too. Each email sending account creates its own handwriting signature style. The sample signature is put on domain `mydomain.example`, where anybody can see the sample, but cannot fake it. Every time the server sends an email, it signs the email using that particular handwriting signature style, and time and place, so that this signature cannot be reuse for any other email. When receiving, the receiver can easily verify whether the signature was done by the legitimate account and service providers, and whether the signature has been tampered with.

See, easy! So grandpa, retire, or stop blacklisting IPs and start using SPF and DKIM!
