---
title: Search for the ultimate device farm
createdDate: '2015-11-23'
updatedDate: '2017-10-10'
author: truongsinh
tags:
  - android
  - ios
  - device
  - test
image: Amazon-AWS-Device-Farm.jpg
draft: false
excerpt: null
---

_Update: we are now using [Applause](https://www.applause.com)_

Mobile app is an inevitable trend now. Our customer support closed all open tickets on his way to work with his phone (by the way, that’s how sweet public transit is, don’t you even think about using your phone while driving). SmarpShare, the first and only employee-first employee advocacy platform, also has mobile app on Android and iOS; and trust me, it’s challenging.

We were excited when releasing our new native app replacing hybrid one back in September, just to learned that, 48 hours later, iOS 9 crashed almost all of our first-comer users. We fixed the problem, but from customer viewpoint, it was catastrophic, because there is no excuse that new iOS upgrade causes incompatible with your app. To make it worse, every update (even hot fix one) needs at least 2 weeks waiting for Apple approval. We learned the hard way that, in every upcoming release, we will have to test our app against beta version of the OS, be it Android 6/7 or iOS 9.2/10.x. Easier said than done, especially when it comes to Android, and its fragmentation.

![God bless us!](AndroidFragmentationTNW.gif)

God bless us!

There’s no way we can afford to buy all Android devices to cover at least [80%](https://en.wikipedia.org/wiki/Pareto_principle) of our users, but luckily, thanks to the cloud era, we have cloud-based device farm. There is no(t yet) definition for this term, but to quote from AWS Device Farm,

> Device Farm is an app testing service that enables you to test your Android, iOS, and Fire OS apps on real, physical phones and tablets that are hosted by Amazon Web Services (AWS). A test report containing high-level results, low-level logs, pixel-to-pixel screenshots, and performance data is updated as tests are completed.

Okay, in simpler English, we **rent** physical devices (in contrast to **buy** them) to test our app. It’s cost-efficient and productive. Our criteria for the device farm are:

-   Stability: the platform itself must be stable enough, so that our engineers/testers don’t have to spend time with their customer support, or worst, figure how to work around platform bugs themselves.
-   Bleeding edge: the farm must have latest, even beta, hardware and software. As described, we learn it the hard way to test our app against beta hardware and software.

So far, we have some candidates

-   [AWS Device Farm](https://aws.amazon.com/device-farm/): stable, but have no Android 6 nor iOS 9.1 (oh, another btw, they have Fire OS, which in our situation, is absolutely irrelevant)
-   [TestObject](https://testobject.com/): bleeding edge, but buggy (at least during our the free trial)
-   [TestDroid](http://testdroid.com/): have 1 device Android 6 (I wish they had more) and iOS 9.1, our impression is that they are quite professional and expert, judging from their [blog posts](http://testdroid.com/blog).

We are trying out [TestDroid](http://testdroid.com/), but always keep our mind open. Have any suggestions? Please leave it in the comment.
