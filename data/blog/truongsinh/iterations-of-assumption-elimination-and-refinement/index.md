---
title: >-
  We need more information… or do we? Iterations of assumption, elimination and
  refinement
createdDate: '2017-10-09'
updatedDate: '2017-10-10'
author: truongsinh
tags:
  - leadership
  - decision making
image: helloquence-61189.jpg
draft: false
canonical: >-
  https://www.linkedin.com/pulse/we-need-more-information-do-iterations-assumption-tran-nguyen/
excerpt: >-
  practicing assumption (list all/most probable outcomes of pending questions),
  elimination (remove all irrelevant questions) and refinement (add more
  relevant questions) in iterations (repeatedly until there are nothing else to
  change) would save you a lot of time gathering information and making
  decisions.
---

A while ago, I wrote an article titled [Stop saying, "it depends"; make your answers useful](https://www.linkedin.com/pulse/stop-saying-depends-make-your-answers-useful-truongsinh-tran-nguyen/). This one can be considered its sequel.

First of all, I was intrigued by [a puzzle](https://www.youtube.com/watch?v=M3NQE0BaAQw) by [Presh Talwalkar](https://www.linkedin.com/in/presh/)

> Surveys have found 80 percent get this logic problem wrong. Alice is looking at Bob, and Bob is looking at Charlie. Alice is married; Charlie is not. Is a married person looking at an unmarried person? Answer choices are A) Yes, B) No, and C) Cannot be determined. The video presents the correct answer.

Stop here if and go to [the puzzle video](https://www.youtube.com/watch?v=M3NQE0BaAQw) if you want to have a fun break.

Welcome back! Sometimes, we think we need more information to proceed, but in fact we don't. Sometimes, we indeed need more, but again we don't have to wait to proceed (read, proceed, not make decisions).

In any issues, if we take a step back, look at the pending questions, list all (or most probable) answers (**_assumption_**), and evaluate our course of actions on such answers, you will see that some of them are actually irrelevant (**_elimination_**). Even if others are relevant, you can already proceed with the hypothesis "if answer to question A is X, we need to do U, if answer to question B is Y, we need to do W", meaning that you have more time to prepare for those plans, especially in the situation "more information" would only be available much later.

Furthermore, it is also common that we are greedy, and tend to gather/obtain all information. It, however, could be legally or technically infeasible (think blood type for treating a scratch, or log everything to debug an app). In another words, we don't know how to ask the right questions. With the above approach, not only can you reduce the number of irrelevant questions, but during the process, you might come up with new relevant questions (**_refinement_**), and then you can repeat the process until no further elimination nor refinement can be made (**_iteration_**). So, instead of waiting for 2 months to confirm that "A is X", figuring out we need to measure "F", waiting for another 2 months, and figuring out we have to measure G and waiting for another 2 (totally half a year to gather needed info to make decision), we can instead measuring those 3 and having the info ready in 2 months.

Subsequent refinements can be the result of any of the combinations of previous refinements and/or eliminations (and not even in the same iteration), and the same goes for subsequent eliminations.

On a side note, regardless of the information you have, **making decision as late as possible** (agile value #4). The iteration of assumption, elimination and refinement I discuss above is to proceed without be blocked, not to make ill-informed cost-of-opportunity-expensive decisions.

So, in a nutshell, practicing assumption (list all/most probable outcomes of pending questions), elimination (remove all irrelevant questions) and refinement (add more relevant questions) in iterations (repeatedly until there are nothing else to change) would save you a lot of time gathering information and making decisions.

It is unfortunate that I cannot provide examples from my own experiences, due to NDA, but you can checkout lots of counter-intuitive problems on Youtube that in the end they are variable-independent, such as [Napkin ring problem](https://www.youtube.com/watch?v=J51ncHP_BrY).

Further reading:

-   [Natural deduction](http://www.cs.cornell.edu/courses/cs3110/2013sp/lectures/lec15-logic-contd/lec15.html)
-   [Agile values and principles](http://bfy.tw/ENkf)
