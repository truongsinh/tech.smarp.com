---
title: >-
  How I wasted my CPU cycles on Go (JSON)RPC, TCP based, client calling Node
  based HTTP RPC server?
createdDate: '2015-11-30'
updatedDate: '2017-10-10'
author: hussain
tags:
  - golang
  - json
  - rpc
  - http
  - tcp
  - performance
image: header.png
draft: false
excerpt: >-
  Go’s RPC package does not provide support to call HTTP based RPC services. You
  can only access RPC services that are provided over the TCP layer.
---

**Take away/tl;dr:**

Go’s (JSON)RPC package does not provide support to call HTTP based RPC services. It is restricted, or, to better describe it, can only access RPC services that are provided over the TCP layer. [TCP is faster than HTTP](http://stackoverflow.com/a/19640412/1309352). If you want to access HTTP based RPC service you have to fallback to using the [HTTP](https://golang.org/pkg/net/http) package to make regular POST based RPC requests to the server. It’s [unlikely](https://github.com/golang/go/issues/2738#issuecomment-66061729) to be made part of the standard library either [because](https://codereview.appspot.com/10704046/#msg2)

> there is no agreed-upon meaning for “jsonrpc over http”. Instead everyone seems to define their own slightly different one. It’s best to leave to third-party libraries.

* * *

## Background:

The journey started with the implementation of a simple Node based script which would perform some task that it, inherintly and apparently, is good at. Had to do with html tags etc and there wasn’t any decent Go library that would do that. Worst, the Node library was pretty lousy as well, had to fix plenty of bugs and unhandle edge cases. And I digressed.

The script was working perfectly through the cli and os/exec (plan on writing the next blog on the problem I had with that and what I discovered therein). But that aint no neat. So, we decided to create a microservice out of that script and invoke it through RPC (language agnostic). Easy-pezy. Well, yeah it was on Node side. Used `node-json-rpc` and `addMethod` the required function. Made curl request to it for smoke-testing (`curl --data '{ "method" : "add", "params" : [ 1, 2 ], "id" : 123 }' http://localhost:1234)`. All was good.

## Problem:

Off I went to our backend (Go based) to prepare the client-side. Found the sample code that did the sum of two number in Go’s [RPC package](https://golang.org/pkg/net/rpc/) doc. It seemed simple so I borrowed the sample, tweaked it a bit and `go run /tmp/gotesting/proc.go`ed it.

The code never proceeded beyond [`rpc.DialHTTPPath`](https://golang.org/pkg/net/rpc/#DialHTTPPath) or [`rpc.DialHTTP`](https://golang.org/pkg/net/rpc/#DialHTTP). The error I got was:

    dial-http tcp localhost:1234: unexpected EOF

or, when I tested it with [Gurujsonrpc’s](http://gurujsonrpc.appspot.com/) test RPC service:

    dial-http tcp gurujsonrpc.appspot.com:http: unexpected HTTP response: 405 Method Not Allowed

Point was that the DialHTTP did access the port serving the RPC but just that it could not reach the RPC service. Entering a different port would return

    dial tcp [::1]:12345: getsockopt: connection refused

This is when I discovered the [`rpcinfo`](http://www.linuxcommand.org/man_pages/rpcinfo8.html) CLI command and that it too only probed TCP (`-t` flag) and UDP (`-u` flag).

## Solution:

Fallback to the using the HTTP package and making Post request to the RPC server. Lifted code [from](https://gist.github.com/fredhsu/11228769) (through the [_Arista eAPI (JSON-RPC over HTTP) in Go_](https://fredhsu.wordpress.com/2014/04/25/arista-eapi-json-rpc-over-http-in-go/) blog) and tweaked. Converted the `Parameters` type to `[]string` so that `json.Marshal` would spit it out as an array instead of a json (the test server expected position based argument).

Things finally work.

## More on the finding:

-   The [json-rpc2](https://www.npmjs.com/package/json-rpc2) library was pretty neat. Especially its debug option (`export DEBUG=jsonrpc`)
-   [node-jsonrpc-tcp](https://github.com/jaredhanson/node-jsonrpc-tcp) provides RPC service over TCP instead of HTTP
-   Project [json-rpc-http](https://github.com/JamesMcMinn/json-rpc-http) for makes RPC calls over HTTP. Seems rudimentary since it only has a single commit, a single file (`rpc.go`) and no tests
