---
title: 'Performance of Go Channels vs. Locks, in read intensive scenarios'
createdDate: '2016-01-24'
updatedDate: '2017-10-10'
author: hussain
tags:
  - golang
  - concurrency
  - thread
  - lock
  - mutex
  - performance
image: 1-jdQldZxDnMPnkxTyl-xgNg.png
draft: false
excerpt: >-
  If you have intense reads and insignificant writes over a particular variable
  across coroutines then you are better off using Go’s Read-Write locking
  mechanism instead of channels, if performance is crucial and data race is
  unacceptable.
---

## Take away/tl;dr

If you have intense reads and insignificant writes over a particular variable across coroutines then you are better off using Go’s Read-Write locking mechanism instead of channels, if performance is crucial and data race is unacceptable.

## Background

Our platform’s middleware function references a common map (`map[string]string`) which is used for caching. Last week, I discoverd that the map was subject to data race and to fix I used channels. But since the middleware is used intensively, performance was a concen. [Sinh](https://www.linkedin.com/in/truongsinh) asked why not use [`RWMutex`](https://golang.org/pkg/sync/#RWMutex) and whether using one over the other would have any performance benefits.

## Benchmarking

Go’s benchmarking feature was used `go test -bench . --benchmem`.

Test function using locks:

Test function using channels:

The `toRead` function was randomly decided whether the function would read or write. Bias was injected into the randomness with through `readPercentage` which was populated the value in `READ_PERCENTAGE` environment variable.

Benchmarking test functions for channels and locks:

Benchmarking was done by varying the read percentage from 1 to 100.

## Results

1.  On Mac, with 4 processors and 4GB memory, `b.N` was automatically decided by the benchmarking tool ![bnAutoMac](https://web.archive.org/web/20160530170607im_/http://tech.smarp.com/wp-content/uploads/2016/01/plot5.png)
2.  On same Mac, with `b.N` set to 5000000 ![bn5000000Mac](https://web.archive.org/web/20160530170607im_/http://tech.smarp.com/wp-content/uploads/2016/01/plot3.png)
3.  On Docker Ubuntu, 1 processor and 2GB memory. `b.N` was set to 500000. ![bn500000P1Ubuntu](https://web.archive.org/web/20160530170607im_/http://tech.smarp.com/wp-content/uploads/2016/01/plot.png)
4.  On Docker Ubuntu, same config except, that 2 processors and ~3GB memory was used ![bn500000P2Ubuntu](https://web.archive.org/web/20160530170607im_/http://tech.smarp.com/wp-content/uploads/2016/01/plot1.png)
5.  On Docker Ubuntu, same config except, that, 4 processors were usedsa

## Conclusions

-   Whether it is read or write, logically the performance would not change if channels were used because in both case the data is extracted from the channel and put back in.
-   RWLock are expensive if the processing power is in abundance and the read percentage is 40-80%. Above and below that range using channels is more performance friendly.
-   On low-memory system the channels have a large memory footprint.

## Ending

Python was used to format the result of the benchmark and RStudio to plot the graph.  
Source code can be found [here](https://github.com/hus787/chanlockbench).
