---
title: Localization and Internationalization on NativeScript
createdDate: '2016-05-20'
updatedDate: '2017-10-10'
author: dinh
tags:
  - i18n
  - l10n
  - javascript
image: grails_i18n_logo.png
draft: false
excerpt: null
---

## Introduction

Hello, I joined Smarp 4 months ago as a Mobile Developer and my very first task is to implement the app to multi-language. I was hopelessly looking for library or plugin that could help me to do the task. All I found out was just a plugin that was outdated and could not be used anymore (the last modified date was like nearly one year ago, damn). Fortunately, my head of tech had brought the light to the tunnel by suggesting me to use the GNU gettext and I have implemented the task successfully up to this moment. And now I’m writing this so that I could hopefully help anyone out there who get troubles with localization on NativeScript.

## Setting things up

Firstly, [Gettext](https://web.archive.org/web/20160530170329/http://www.gnu.org/software/gettext/) is a translation standard which is used to solve the problems of writing program in multi-human-language. There are many implementations of gettext in nearly every programming language. For this task, I will use [Jed](https://web.archive.org/web/20160530170329/https://www.npmjs.com/package/jed) as it seems to be the best quality and speed JavaScript implementation at this moment. We also need a tool to compile .po file into .json so it can read keys and values of those translation. So to set up the gettext:

    brew install gettext
    npm install jed
    npm install po2json

Secondly, prepare this piece of code in the **app.ts** (I’m using Typescript so the all the code I mention in this post is in Typescript). There are many gettext function in jed but to simplify, I will only use `gettext()`:

As you can see, the function uses a .json file as a dictionary to look up the values based on keys. In this task, I will translate the app from English to French and because I don’t have any idea about French, I will need a translator to help me translate English things. Sadly, not all of translators are tech people so setting up things for them to translate in .json are kinda painful as it’s really scary to non-tech people. That’s why we need PO file, plus it’s would be much easier to manage those strings in the future. I suggest to have one template file that follow this format (these strings are just examples), just create a text file and name it with the ending `.pot`:

One more thing, you will need to have [PO editor](https://web.archive.org/web/20160530170329/https://poedit.net/) to edit those PO files more easily. You will use PO editor to open the POT file that just created and start the new translation in any languages you would like to. Now, we should have a po file created (let’s name it `fr.po`) and this file will need to be compile to json. It’s time to use poj2son module, and you have almost done the preparation:

    po2json --format=jed1.x fr.po fr.json

## Implementation

Next step is to bring those translation to the app. In the XML file, put it like this in anywhere you want the translation to be implemented:

In the code-behind-page file (.ts, .js), if the page does not have the view-model, you just need to create a new Observable object, make it be the page.bindingContext and set the text of the label into a new value like this:

If the page has the view-model, you should implement it inside the viewmodel file instead of .js file. Similarly, if any component of XML has its own bindingContext, you will have to do the translation inside the its viewmodel class:

As you can see, the function `sprintf(_(""))` is a global function that will look up the `.json` that is loaded when the app starts. You can use this to translate UI component, dialog pop-up, and error messages. From this point, I can start implement other languages in the future by just preparing a PO file, compile it into json and the watch the app changing to another language without touching any code files. When you have the new strings that need to be translated, just add them into the POT file as the keys, after that open the po file with PO editor –> choose “Catalog” tab –> “Update from POT file” and start translating.

## Conclusion

It took around more than one month for me to finish this task and my app now could be changed to 3 different languages. The most troublesome things are those XML pages which have viewmodel with 2 or 3 components have different bindingContext. The most valuable thing is that to finish this, I had to go through all over the app (every single pages) and it’s really useful as I just started here as my very first IT job. I’m happy to know that they are doing a plugin to internationalize the app for NativeScript but I don’t know when would it be released. Hopefully this post can help those who are in urgent and can’t wait for the plugin.
